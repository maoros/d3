var hum =[150, 140, 130, 220, 210, 190, 190, 175, 165, 150];

var svgWidth = 300, svgHeight = 330, barPadding = 10;
var barWidth = (svgWidth / hum.length);

var svg = d3.select('svg')
    .attr("width", svgWidth)
    .attr("heigth",svgHeight);

var yScale = d3.scaleLinear()
    .domain([0, d3.max(hum)])
    .range([0, svgHeight]);

var barChart = svg.selectAll("rect")
    .data(hum)
    .enter()
    .append("rect")
    .attr("y", function(d) {
        return svgHeight - yScale(d)
    })
    .attr("height", function(d){
        return d;
    })
.attr("width", barWidth - barPadding)
.attr("transform", function (d, i){
    var translate = [barWidth * i, 0];
    return "translate("+ translate +")";
});

var text = svg.selectAll("text")
    .data(datos)
    .enter()
    .append("text")
    .text(function(d) {
        return d;
    }) 
.attr("y", function(d, i){
    return svgHeight - d - 2;
})
.attr("x", function(d, i){
    return barWidth * i;
})
.attr("fill", "#A64C38");